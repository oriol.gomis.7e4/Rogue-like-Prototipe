﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jerro : MonoBehaviour
{
    private GameManager gm;
    private GameObject obj;
    // Start is called before the first frame update
    void Start()
    {
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Impact() 
    {
        obj = gm.drop;
        Instantiate(obj, transform.position, Quaternion.identity);
        Destroy(this.gameObject);

       
    }


}
