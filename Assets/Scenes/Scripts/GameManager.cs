﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    
    public Player player;
    private int Score;
    private int mobsKilled;
    private GameObject spawners;
    public GameObject drop;
    public List<GameObject> map;
    public int MapCounter;
    public int totalEnemies;
    [SerializeField]
    private GameObject portal;
    [SerializeField]
    private GameObject door;

    public GameObject shop;
    public UiController ui;
    [SerializeField]
    private MaxScore mscr;
    public static GameManager instance = null;
    

    private void Awake() {

        if (instance!=null && instance!=this)
        {
            Destroy(this.gameObject);
        }

        instance = this;
        //DontDestroyOnLoad(this.gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Start GM!");
        initValues();

    }

    private void initValues()
    {
        //Activa mapa corresponent (carregant les variables de porta i poratl.
        Debug.Log("init");
        totalEnemies = 0;
        MapCounter = 0;
        player = GameObject.Find("Player").GetComponent<Player>();

        spawners = GameObject.Find("Spawners");
        map = gameObject.GetComponent<List<GameObject>>();
        door = map[MapCounter].transform.GetChild(6).gameObject.GetComponent<GameObject>();
        portal = map[MapCounter].transform.GetChild(5).gameObject.GetComponent<GameObject>();

        portal.SetActive(false);
        Debug.Log("a" + map[MapCounter].transform.GetChild(6).name.ToString());

        
        mscr = gameObject.GetComponent<MaxScore>();
    }

    // Update is called once per frame
    void Update()
    {
        
        player = GameObject.Find("Player").GetComponent<Player>();
        door = map[MapCounter].transform.GetChild(6).gameObject.GetComponent<GameObject>();
        portal = map[MapCounter].transform.GetChild(5).gameObject.GetComponent<GameObject>();

        
        
        if (player.life==0) {
            spawners.SetActive(false);
            totalEnemies = 0;
            
        }

        if (Score > mscr.score)
        {
            mscr.score = score;
        }

    }

    public int score
    {
        get
        {
            return this.Score;

        }
    
    }

    public int kills
    {
        get
        {
            return this.mobsKilled;

        }
    }

    public void sumarScore(int score) {
        Score += score;
    }

    public void restarVida(int vida)
    {
        player.life -= vida;
        if (player.life==0) {
            player.isdead = true;
            player.die();
        }

    }

    public void monsterkilled(int num) {
        mobsKilled += num;
    }

    public GameObject ItemDrop() {
        
        return drop;
    }

    public void Portal() {
        map[MapCounter].SetActive(false);
        shop.SetActive(true);
        player.transform.position = new Vector3(5f,1f,-2f);
    }

    public void PortalShop() {
        map[MapCounter].SetActive(true);
        shop.SetActive(false);
        player.transform.position = new Vector3(0f, 0f, -2f);
    }

    public void nextRoom() {
        map[MapCounter].SetActive(false);
        MapCounter++;
        map[MapCounter].SetActive(true);
        player.transform.position = new Vector3(0f, 0f, player.transform.position.z);
    }

    public void endRound() {
        if (MapCounter == 2 && totalEnemies == 20)
        {
            ui.finalscene();
            totalEnemies = 0;
            
        }
        else
        {
            map[MapCounter].transform.GetChild(5).gameObject.SetActive(true);
            map[MapCounter].transform.GetChild(6).gameObject.SetActive(true);

            totalEnemies = 0;
        }

    }



}
