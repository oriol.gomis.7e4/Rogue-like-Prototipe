﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Coins", menuName = "ScriptableObjects/Coins", order = 1)]

public class Coins : Item
{
    public float coins;
}
