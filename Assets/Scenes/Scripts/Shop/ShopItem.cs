﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "ItemShop", menuName = "ScriptableObjects/ItemShop", order = 1)]
public class ShopItem : Item
{
    public string name;

    public float price;

    public string _descripcion;

    public float effect;

    public Sprite icon;

}
