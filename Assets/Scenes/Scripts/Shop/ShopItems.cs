﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Items", menuName = "ScriptableObjects/ShopItems/Item", order = 1)]

public class ShopItems : Item
{
    public List<ShopItem> shopitems;
}
