﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemsOfShop : MonoBehaviour
{
    // Start is called before the first frame update

    public SpriteRenderer sr;
    private Player player;
    public ShopItems SI;
    private GameManager gm;
    private string nameit;


    void Start()
    {
        player = GameObject.Find("Player").GetComponent<Player>();
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();

        int a = Random.Range(0, 3);
        Debug.Log(a);
        sr.sprite = SI.shopitems[a].icon;
        nameit = SI.shopitems[a].name;
    }

    // Update is called once per frame
    void Update()
    {
        
        
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player")) 
        {
            if (SI.shopitems[0].name.Equals(nameit))
            {
                Debug.Log(SI.shopitems[0].name);

                if(player.coins >= SI.shopitems[0].price) 
                {
                    player.life = player.life + (int)SI.shopitems[0].effect;
                    player.coins = player.coins- SI.shopitems[0].price;
                    Destroy(this.gameObject);
                } 
                
            }

            if (SI.shopitems[1].name.Equals(nameit))
            {
                Debug.Log(SI.shopitems[1].name);
                if (player.coins >= SI.shopitems[1].price)
                {
                    player.speed =+SI.shopitems[1].effect;
                    player.coins =player.coins - SI.shopitems[1].price;
                    Destroy(this.gameObject);
                }
                
                
            }

            if (SI.shopitems[2].name.Equals(nameit))   
            {
                Debug.Log(SI.shopitems[2].name);
                if (player.coins >= SI.shopitems[2].price)
                {
                    gm.sumarScore((int)SI.shopitems[2].effect);
                    player.coins = player.coins - SI.shopitems[2].price;
                    Destroy(this.gameObject);
                }
            }

        }
    }

}

