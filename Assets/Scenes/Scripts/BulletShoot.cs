﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletShoot : MonoBehaviour
{
    // Start is called before the first frame update

    public float speed = 40f;
    public Rigidbody2D rb;
    public GameObject bulletprefab;
    int damage = 1;
    private GameManager gm;
    private Jerro jr;
    // Update is called once per frame
    void Start()
    {
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
        rb.velocity = transform.right * speed;
        jr = GameObject.FindWithTag("gerro").GetComponent<Jerro>();
    }
    
    void OnTriggerEnter2D(Collider2D collider)
    {
        Enemy enemy = collider.GetComponent<Enemy>();
        if (collider.CompareTag("enemy"))
        {
           enemy.TakeDamage(damage);
           gm.sumarScore(5);
           gm.monsterkilled(1);
           Destroy(gameObject);
        }
        
        if (collider.CompareTag("gerro")) 
        {
            jr.Impact();
            Destroy(gameObject); 
        }

        if (collider.CompareTag("wall"))
        {
            Destroy(gameObject);
        }
    }
}
