﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField]
    private GameObject enemy;
    
    private Player player;

    public int cont = 0;
    public int maxEnemeis = 5;
    private GameManager gm;
    public Vector2 randomX;
    public Vector2 randomY;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player").GetComponent<Player>();
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
        InvokeRepeating("CreateEnemy", 1, 5);
        


    }

    // Update is called once per frame
    void Update()
    {
        if (player.isdead==true) 
        {
            cont = maxEnemeis;
        }
    }
    private void CreateEnemy()
    {
        if (gm.MapCounter == 2)
        {
            if (cont < maxEnemeis)
            {

                Instantiate(enemy, new Vector3(transform.position.x, transform.position.y, 0), Quaternion.identity);
                cont++;
            }
        }

        else 
        {
            if (cont < maxEnemeis)
            {
                Instantiate(enemy, new Vector3(Random.RandomRange(randomX.x, randomX.y), Random.RandomRange(randomY.x, randomY.y), 0), Quaternion.identity);
                cont++;
            }
        }
        
    }
}
