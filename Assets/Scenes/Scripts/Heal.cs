﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heal : MonoBehaviour
{

    public HealHearts healScriptObj;
    private Player player;
    private GameManager gm;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        StartCoroutine(CD());
    }


    public void OnTriggerEnter2D(Collider2D collider2D) {

        if (collider2D.CompareTag("Player")) {
            Debug.Log("heal" +player.life);
           
            if (player.life>=100) {
                gm.sumarScore(10);
                
            }
            else {
                player.life += healScriptObj.heal;
                
            }
            Destroy(this.gameObject);




        }
    }
    IEnumerator CD() {
       
        yield return new WaitForSeconds(10.0f);
        Destroy(this.gameObject);
    }
    
}
