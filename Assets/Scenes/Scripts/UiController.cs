﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UiController : MonoBehaviour
{
    // Start is called before the first frame update
    public Text score;
    public Text Health;
    public Text finalscore;
    public Text mobskilled;
    public Text finalCoins;
    public Text coins;
    private GameManager gm;
    
    public GameObject results;
    public GameObject final;
    private Player player;
    private EnemySpawner ES;

    public Text totalmosters;
    public Text totalcoins;
    public Text totalscore;
   
    void Start()
    {

        results = GameObject.Find("ResultScene");
        results.SetActive(false);
        final = GameObject.Find("FinalScene");
        final.SetActive(false);
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
        player = GameObject.Find("Player").GetComponent<Player>();

    }

    // Update is called once per frame
    void Update()
    {

        score.text = "Score: " + gm.score;
        Health.text = "Health: " + player.life;
        finalscore.text = "Your score is :" + gm.score;
        mobskilled.text = "You killed " + gm.kills + " monsters";
        coins.text = "Coins :" + player.coins + ". ";
        finalCoins.text = "You end round with: " + player.coins + " coins.";

        if (player.life == 0 || gm.totalEnemies == 20)
        {
            if (player.life == 0)
            {
                Result();
            }
            else 
            {
                Result();
                gm.endRound();

            }
        }
    }

    public void Result()
    {
       results.SetActive(true);
    }

    public void Restart()
    {
        SceneManager.LoadScene("Game");
    }

    public void closeResults()
    {
        gm.totalEnemies = 0;
        results.SetActive(false);
    }

    public void mainmenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void finalscene()
    {
        totalmosters.text = "Monsters: " + gm.kills;
        totalcoins.text = "Coins: " + player.coins;
        totalscore.text = "Score: " + gm.score;
        final.SetActive(true);
    }


}

