﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    // Start is called before the first frame update
    private Player player;
    private int  health;
    private GameManager gm;
    public GameObject obj;
    void Start()
    {
        player = GameObject.Find("Player").GetComponent<Player>();
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
        health = 1;
    }

    // Update is called once per frame
    void Update()
    {
        if (player.isdead==true) { 
            Die(); 
        }
        transform.position = Vector3.MoveTowards(transform.position,player.transform.position,4f*Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            TakeDamage(1);
            gm.restarVida(25);
            
        }
    }


    public void TakeDamage(int dmg)
    {
        health -= dmg;
        if (health <= 0)
        {
            gm.totalEnemies += 1;
            Die();
            Instantiate(obj, transform.position, Quaternion.identity);

        }
    }

    void Die()
    {
        Destroy(this.gameObject);
    }
}
