﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    // Start is called before the first frame update

    public Text bestscore;
    [SerializeField]
    private MaxScore mx;
    void Start()
    {
        bestscore.text = "Best Score: "+ mx.score+" pts.";
    }

    // Update is called once per frame
    void Update()
    {
    }

    void start() 
    {
    }

    public void Play()
    {
        SceneManager.LoadScene(1,LoadSceneMode.Single);
    }
}
