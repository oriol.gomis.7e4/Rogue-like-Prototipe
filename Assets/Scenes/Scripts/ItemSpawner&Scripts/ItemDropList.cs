﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ItemListDrop", menuName = "ScriptableObjects/ItemListDrops", order = 1)]
public class ItemDropList: ScriptableObject
{
    
    // Start is called before the first frame update
    public List<ItemDataSC> items;

    public GameObject ItemDropablePrefab;
    public GameObject getRandomItem() {
        //Random funcionality
        // random que fa que apareixi un item random dins d'una list
        GameObject item = Instantiate(ItemDropablePrefab);
        ItemDataSC itemData =items[Random.Range(0, items.Count)];
        item.GetComponent<SpriteRenderer>().sprite = itemData.Icon;
         

        return item;
    }
}
