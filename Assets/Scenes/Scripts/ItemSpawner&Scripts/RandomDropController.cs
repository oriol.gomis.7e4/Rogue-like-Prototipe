﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomDropController: MonoBehaviour
{

    [SerializeField]
    private ItemDropList itemsList;
    public List<ItemDataSC> items;
    // Start is called before the first frame update
    void Start()
    {
        Invoke("getRandomItem4Invoke", 1);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void getRandomItem4Invoke() {
        GameObject item = itemsList.getRandomItem();
        item.transform.position = this.gameObject.transform.position;
    }
}
