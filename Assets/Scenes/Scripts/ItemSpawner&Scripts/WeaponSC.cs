﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="WeaponData", menuName ="ScriptableObjects/WeaponData",order =1)]
public class WeaponSC : ItemDataSC
{
    public int Damage;
    public int BulletAmount;
    public int MaxBulletCapacity;

    public string Name;
    public float FireRate;
    public int BulletXShoot;
    public float ReloadTime;
}
