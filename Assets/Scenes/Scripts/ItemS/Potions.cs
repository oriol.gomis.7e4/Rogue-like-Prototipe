﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Potions", menuName = "ScriptableObjects/Items/ConsumibleItem/Potions", order = 1)]

public class Potions : Consumible_Item
{
    public int heal;
    public float damageBoost;
    public float manaRegen;
    public Sprite sp;
}
