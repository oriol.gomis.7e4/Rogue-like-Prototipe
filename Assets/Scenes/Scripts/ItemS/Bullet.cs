﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Bullet", menuName = "ScriptableObjects/Items/ConsumibleItem/Bullet", order = 1)]

public class Bullet : Consumible_Item
{
    public int damage;
    public int numberOfBullets;
    public float bulletSize;

}
