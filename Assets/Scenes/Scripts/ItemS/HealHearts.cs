﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Hearts", menuName = "ScriptableObjects/Items/ConsumibleItem/Hearts", order = 1)]

public class HealHearts : Consumible_Item
{
    
    public int heal;
}
