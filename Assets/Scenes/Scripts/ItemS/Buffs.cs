﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Buffs", menuName = "ScriptableObjects/Items/PassiveItems/Buffs", order = 1)]

public class Buffs : Passive_Items
{
    public float TimeActive;
    
}
