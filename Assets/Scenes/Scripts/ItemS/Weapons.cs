﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Weapons", menuName = "ScriptableObjects/Items/ActiveItems/Weapons", order = 1)]

public class Weapons : Active_Items
{
    public int damage;
    public float AttackSpeed;
    public int Ammo;
    public Sprite sp;
}
