﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class Item : ScriptableObject
{
    //[SerializeField]
    private string _name;
    //[SerializeField]
    private float _price;
    //[SerializeField]
    private string _item_type;
    //[SerializeField]
    private Sprite _image;

    public string Name {
        get {
            return this._name;
        }
    }
    
    public float Price {
        get {
            return this._price;
        }
    }
    public string Item_type
    {
        get
        {
            return this._item_type;
        }
    }
    public Sprite Image
    {
        get
        {
            return this._image;
        }
    }

}
