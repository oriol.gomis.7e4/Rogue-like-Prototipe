﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Character
{
    // Start is called before the first frame update
    //public Sprite[] spr;
    public SpriteRenderer Sp;
    public int contador = 0;
    public Animator animation;
    public int life=100;
    public bool isdead=false;
    private Camera cam;
    public Rigidbody2D rb;
    public float coins;
    public Coins allcoins;
    public float speed = 0;
    

    
    void Start()
    {
        Debug.Log("Player Start!  " + GameManager.instance.player);

        GameManager.instance.player = this;
        life = 100;
        health=life;
        cam = GameObject.Find("Main Camera").GetComponent<Camera>();
        speed = 0;
    }

    // Update is called once per frame
    void Update()
    {
        Movement();
        allcoins.coins = coins;
    }

    public void Movement() 
    {
        animation.SetBool("Camina", false);
        if (Input.GetKey(KeyCode.W))
        {
            animation.SetBool("Camina", true);
            transform.position += new Vector3(0, 5f+speed) * Time.deltaTime;
        }

        if (Input.GetKey(KeyCode.S))
        {
            animation.SetBool("Camina", true);
            transform.position += new Vector3(0, -5f-speed) * Time.deltaTime;
        }

        if (Input.GetKey(KeyCode.A))
        {
            if (!Sp.flipX == true)
            {
                Sp.flipX = true;
            }
            animation.SetBool("Camina", true);
            transform.position += new Vector3(-5f-speed, 0) * Time.deltaTime; 
        }

        if (Input.GetKey(KeyCode.D))
        {
            if (!Sp.flipX == false)
            {
                Sp.flipX = false;
            }
            animation.SetBool("Camina", true);
            transform.position += new Vector3(5f+speed, 0) * Time.deltaTime;
        }
        
        if (Input.GetKey(KeyCode.Q)) 
        {
            Dash();
        }
    }

    public void die() 
    {
        Destroy(this.gameObject);
    }

    public void Dash() 
    {
       // Funcio que s'ha de aplicar a un objecte que faci d'arma, el que fa es agafar la posicio del mouse i apuntar cap a ella .
        Vector3 mouseposition = cam.ScreenToWorldPoint(Input.mousePosition);
        Vector3 aimdirection = (mouseposition - transform.position).normalized;
        rb.AddForce(aimdirection,ForceMode2D.Impulse); 
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Portal")) {
            GameManager.instance.Portal();
        }

        if (collision.CompareTag("PortalShop"))
        {
            GameManager.instance.PortalShop();
        }

        if (collision.CompareTag("Door"))
        {
            GameManager.instance.nextRoom();
        }
    }

}
