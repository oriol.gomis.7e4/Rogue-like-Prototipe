﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Character_Data : ScriptableObject
{
    private int _health;
    private float _speed;

    public int Health {
        get {
            return this._health;
        }
    }
    public float Speed
    {
        get
        {
            return this._speed;
        }
    }
}
